class Movel(object):
	tipo = ' '
	material = ' '
	tamanho = 0
	cor = ' '

	def __init__(self, tipo, material, tamanho, cor):
		self.tipo = tipo
		self.material = material
		self.tamanho = tamanho
		self.cor = cor

	def set_tipo(self, tipo):
		self.tipo = tipo

	def set_material(self, material):
		self.material = material

	def set_tamanho(self, tamanho):
		self.tamanho = tamanho

	def set_cor(self,cor):
		self.cor = cor

	def imprimir(self):
		print(self.tipo, self.material, self.tamanho, self.cor)

movel = Movel('porta','mdf',3,'branco')
movel.imprimir()

